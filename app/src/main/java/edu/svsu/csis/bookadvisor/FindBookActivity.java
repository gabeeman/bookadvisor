package edu.svsu.csis.bookadvisor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FindBookActivity extends Activity {
    private BookExpert expert = new BookExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_book);
    }

    //Call when the button gets clicked
    public void onClickFindBeer(View view) {
        //Get a reference to the TextView
        TextView brands = (TextView) findViewById(R.id.brands);

        //Get a reference to the Spinner
        Spinner genre = (Spinner) findViewById(R.id.color);

        //Get the selected item in the Spinner
        String bookType = String.valueOf(genre.getSelectedItem());

        //Get recommendations from the BeerExpert class
        List<String> genreList = expert.getGenres(bookType);
        StringBuilder genresFormatted = new StringBuilder();
        for (String brand : genreList) {
            genresFormatted.append(brand).append('\n');
        }

        //Display the beers
        brands.setText(genresFormatted);
    }
}
