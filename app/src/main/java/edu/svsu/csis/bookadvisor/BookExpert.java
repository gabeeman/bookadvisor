package edu.svsu.csis.bookadvisor;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidg on 28/04/2017.
 */

public class BookExpert {
    List<String> getGenres(String genre) {
        List<String> genres = new ArrayList<>();
        if (genre.equals("Fiction")) {
            genres.add("To Kill A Mockingbird");
            genres.add("The Lord of the Rings");
        } else if (genre.equals("Non-Fiction")){
            genres.add("Sapiens: A Brief History of Mankind");
            genres.add("Into Thin Air: A Personal Account of the Mt. Everest Disaster");
        } else if (genre.equals("Horror")){
            genres.add("IT");
            genres.add("Dracula");
        } else if (genre.equals("Drama")){
            genres.add("Pride and Prejudice");
            genres.add("Macbeth");
        }
        return genres;
    }
}
